﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UIManager : MonoBehaviour
{
    [Header("Setup Phase UI")]
    public Transform CountDownText;
    public Transform StateDisplayText;
    public Transform SetupDisplay;
    public Transform LevelCounter;

    [Header("Simulation Phase UI")]
    public Transform Button;

    [Header("Debrief Phase UI")]
    public Transform NextPhaseButton;
    public Transform DebriefPanel;
    public Transform TimeElapsedValue;
    public Transform BuildingsLostValue;
    public Transform PeopleHospitalizedValue;
    public Transform MoneyEarnedValue;
    public Transform TotalHospitalsStuffs;



    private static UIManager _instance;

    public static UIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
