﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Rescuer : Monster
{
    private bool hasRescued = false;

    public override void PerformDuties()
    {
        Vector3 monsterPosition = transform.position;
        float distanceToTarget = Mathf.Infinity;

        // Get closest fire
        GameObject[] fires = GameObject.FindGameObjectsWithTag("Fire"); // probably dont want to do this every frame. should probably also be static class and not done on every object

        if (fires.Length > 0)
        {
            hasTarget = true;

            foreach (GameObject fire in fires)
            {
                Vector3 difference = fire.transform.position - monsterPosition;
                float distance = difference.magnitude;
                if (distance < distanceToTarget)
                {
                    targetFire = fire;
                    distanceToTarget = distance;
                }
            }
        }
        else
        {
            hasTarget = false;
        }

        anim.SetBool("WalkDown", false);
        anim.SetBool("WalkRight", false);
        anim.SetBool("WalkLeft", false);
        anim.SetBool("Idle", false);
        anim.SetBool("Spray", false);

        if (hasTarget)
        {
            // If fire is in spray range, spray
            if (distanceToTarget < sprayRange)
            {
                if (!hasRescued)
                {
                    targetFire.GetComponent<BuildingChild>().daddy.RescuePeople();
                    hasRescued = true;
                }
                navAgent.isStopped = true;
                targetFire.GetComponent<BuildingChild>().GetSprayed(sprayPower * Time.deltaTime);
                anim.SetBool("Spray", true);
                var displacement = targetFire.transform.position - monsterPosition;
                if (!source.isPlaying)
                {
                    source.Play();
                }
                if (!ps.isPlaying)
                {
                    ps.Play();
                }
                if (displacement.x < 0)
                {
                    spriteRenderer.flipX = true;
                    ParticleSystem.transform.rotation = Quaternion.Euler(0, -45, 0);
                }
                else
                {
                    spriteRenderer.flipX = false;
                    ParticleSystem.transform.rotation = Quaternion.Euler(0, 45, 0);
                }
            }
            else // Else, head toward fire
            {
                navAgent.isStopped = false;
                navAgent.SetDestination(targetFire.transform.position);

                if (source.isPlaying)
                {
                    source.Stop();
                }
                if (ps.isPlaying)
                {
                    ps.Stop();
                }

                anim.SetBool("WalkDown", false);
                anim.SetBool("WalkRight", false);
                anim.SetBool("WalkLeft", false);
                anim.SetBool("Idle", false);
                var x = navAgent.velocity.x;
                var z = navAgent.velocity.z;
                if(Math.Abs(z) > Math.Abs(x))
                {
                    if(z < 0) // going down
                    {
                        anim.SetBool("WalkDown", true);
                    }
                    else
                    {
                        if(x > 0 )
                        {
                            anim.SetBool("WalkRight", true);
                        }
                        else
                        {
                            anim.SetBool("WalkLeft", true);
                        }
                    }
                }
                else if(x != 0)
                {
                    if (x > 0)
                    {
                        anim.SetBool("WalkRight", true);
                    }
                    else
                    {
                        anim.SetBool("WalkLeft", true);
                    }
                }
                else
                {
                    anim.SetBool("Idle", true);
                }
            }
        }
        else
        {
            navAgent.isStopped = true;
        }
    }
}
