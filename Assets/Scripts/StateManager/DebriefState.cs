﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.StateManager
{
    public class DebriefState : State
    {
        public DebriefState() 
        {
            name = "Debrief";
        }

        private bool showOnce = true;
        private float elapsedTime;
        private float preDebriefTime = 1.05f;

        private bool goToNextState = false;

        public override bool checkForTransition()
        {
            return goToNextState;
        }

        public override void setup()
        {
            elapsedTime = 0.0f;
            goToNextState = false;
            showOnce = true;
        }

        public override void update()
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= preDebriefTime && showOnce)
            {
                EventManager.TriggerEvent("StartDebrief", null);
                showOnce = false;
                goToNextState = false;
                RoundSummary rs = GameManager.Instance.GetRoundSummary();
                PlayerManager.Instance.AddMoney(rs.MoneyEarned);
                //hide and show UI
                UIManager.Instance.NextPhaseButton.gameObject.SetActive(true);
                UIManager.Instance.NextPhaseButton.GetComponent<Button>().onClick.AddListener(skipPhase);

                UIManager.Instance.DebriefPanel.gameObject.SetActive(true);
                UIManager.Instance.TimeElapsedValue.GetComponent<TextMeshProUGUI>().text = $"{rs.TimeElapsed}s";
                UIManager.Instance.BuildingsLostValue.GetComponent<TextMeshProUGUI>().text = rs.BuildingsLost.ToString();
                UIManager.Instance.PeopleHospitalizedValue.GetComponent<TextMeshProUGUI>().text = rs.PeopleHospitalized.ToString();
                UIManager.Instance.MoneyEarnedValue.GetComponent<TextMeshProUGUI>().text = $"${rs.MoneyEarned}";
                UIManager.Instance.TotalHospitalsStuffs.GetComponent<TextMeshProUGUI>().text = "Total Hospitilized " + GameManager.Instance.totalHospitlized + " / " + GameManager.Instance.hospitalLimit + " Total Capacity";
            }
            //Tick Timer
        }

        public override void cleanUp()
        {
            UIManager.Instance.NextPhaseButton.gameObject.SetActive(false);
            UIManager.Instance.NextPhaseButton.GetComponent<Button>().onClick.RemoveListener(skipPhase);
            UIManager.Instance.DebriefPanel.gameObject.SetActive(false);
            EventManager.TriggerEvent("EndDebrief", null);
        }

        public void skipPhase()
        {
            if(GameManager.Instance.totalHospitlized > GameManager.Instance.hospitalLimit)
            {
                SceneManager.LoadScene(2);
            }
            else if(GameManager.Instance.level > 10)
            {
                SceneManager.LoadScene(3);
            }

            goToNextState = true;
        }
    }
}
