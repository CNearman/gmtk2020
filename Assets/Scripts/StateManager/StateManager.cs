﻿using Assets.Scripts.StateManager;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.StateManager
{
    public class StateManager : MonoBehaviour
    {
        private static StateManager _instance;
        private State currentState;

        public AudioClip boop1;
        public AudioClip boop2;
        public AudioSource aSource;
        
        public static StateManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            State.SETUP.targetState = State.SIMULATION;
            State.SIMULATION.targetState = State.DEBRIEF;
            State.DEBRIEF.targetState = State.SETUP;

            currentState = State.SETUP;
            currentState.setup();

            // TODO: Go Away
            UIManager.Instance.StateDisplayText.GetComponent<TextMeshProUGUI>().text = currentState.name;
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log("Current State: " + currentState.name);
            currentState.update();
            if (currentState.checkForTransition())
            {
                currentState.cleanUp();
                currentState = currentState.targetState;
                currentState.setup();

                // TODO: Go Away
                UIManager.Instance.StateDisplayText.GetComponent<TextMeshProUGUI>().text = currentState.name;
            }
        }
    }
}
