﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingChild : MonoBehaviour
{
    public Building daddy;

    // Start is called before the first frame update
    void Start()
    {
        daddy = gameObject.transform.GetComponentInParent<Building>();
    }

    public void SetFireActive()
    {
        gameObject.tag = "Fire";
    }

    public void SetFireDeactive()
    {
        gameObject.tag = "NotFire";
    }

    public void GetSprayed(float power)
    {
        daddy.GetSprayed(power);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
