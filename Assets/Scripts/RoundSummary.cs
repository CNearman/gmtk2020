﻿namespace Assets.Scripts
{
    public class RoundSummary
    {
        public float TimeElapsed { get; set; }

        public int BuildingsLost { get; set; }

        public int PeopleHospitalized { get; set; }

        public int MoneyEarned { get; set; }
    }
}
