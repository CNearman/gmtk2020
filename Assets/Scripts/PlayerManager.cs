﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public int PlayerMoney;

    public Text moneyTracker;
    public Text slotTracker;

    public Canvas canvas;
    public GameObject monsterButton;
    public GameObject monsterStoreButton;
    public float cardStartPos;
    public float cardOffset;

    Dictionary<int, MonsterCard> roster;

    public Camera cam;
    public GameObject testObject;
    public bool deploying;
    public int monsterToDeploy;

    public GameObject emptySlotCard;

    public GameObject BuyNewSlotCard;

    public int maxSlots;
    public int currentSlots;
    public int slotsUsed;

    public float levelStoreScaler;

    public GameObject RosterBox;
    public GameObject StoreBox;

    List<string> names;

    public AudioClip money1;
    public AudioClip money2;
    public AudioSource aSource;

    public struct MonsterClasses
    {
        public string classNotes;
        public GameObject classPrefab;
        public float maxRange;
        public float minRange;
        public float maxPower;
        public float minPower;
        public float maxSpeed;
        public float minSpeed;
        public Color filterColor;
    }

    public List<MonsterClasses> possibleClasses;

    
    [Header("Monsters")]
    public GameObject DefaultMonster;
    public GameObject FarAwayMonster;
    
    private static PlayerManager _instance;
    public static PlayerManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }

        EventManager.StartListening("PrepareToDeploy", PrepareToDeploy);
        EventManager.StartListening("CancelDeploy", CancelDeploy);
        EventManager.StartListening("RecallMonster", RecallMonster);
        EventManager.StartListening("EndDebrief", ResetRoster);
        EventManager.StartListening("BuyAMonster", BuyAMonster);
        EventManager.StartListening("SellAMonster", SellAMonster);
        EventManager.StartListening("StartSimulation", CancelDeploy);
        EventManager.StartListening("BuyASlot", BuyASlot);
    }

    public void AddMoney(int money)
    {
        PlayerMoney += money;
        moneyTracker.text = "Money: $" + PlayerMoney;
        EventManager.TriggerEvent("UpdateBuyPower", new MoneyEvent { money = PlayerMoney, slots = (slotsUsed < currentSlots) });
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        roster = new Dictionary<int, MonsterCard>();
        GenerateNames();
        GenerateClasses();

        /*MonsterCard testMonster = new MonsterCard
        {
            isDeployed = false,
            monsterName = "bob",
            monsterSpeed = 3, // Default was 2. could range from 1 to 6?
            monsterPower = 4, // Default is 4.
            monsterRange = 1,  // default is 1.
            MonsterPrefab = DefaultMonster
        };
        MonsterCard testMonster1 = new MonsterCard
        {
            isDeployed = false,
            monsterName = "sally",
            monsterSpeed = 3, // Default was 2. could range from 1 to 6?
            monsterPower = 6, // Default is 4.
            monsterRange = 2,  // default is 1.
            MonsterPrefab = FarAwayMonster,
            monsterNotes = "Hops to the middle of the map to start."
        };
        MonsterCard testMonster4 = new MonsterCard
        {
            isDeployed = false,
            monsterName = "sally",
            monsterSpeed = 3, // Default was 2. could range from 1 to 6?
            monsterPower = 6, // Default is 4.
            monsterRange = 2,  // default is 1.
            MonsterPrefab = FarAwayMonster,
            monsterNotes = "Hops to the middle of the map to start."
        };
        MonsterCard testMonster2 = new MonsterCard
        {
            isDeployed = false,
            monsterName = "bob"
        };
        MonsterCard testMonster3 = new MonsterCard
        {
            isDeployed = false,
            monsterName = "bob"
        };

        AddToRoster(testMonster);
        AddToRoster(testMonster1);
        AddToRoster(testMonster4);
        AddToRoster(testMonster2);
        AddToRoster(testMonster3);*/

        ShowUpdatedRoster();

        ShowStore(5);

        AddMoney(20);
    }

    public void AddToRoster(MonsterCard monster)
    {
        int recruitId = 0;
        while (roster.ContainsKey(recruitId))
        {
            recruitId++;
        }
        roster.Add(recruitId, monster);

        slotsUsed = roster.Keys.Count;

        slotTracker.text = "Slots: " + slotsUsed + "/" + currentSlots;

        ShowUpdatedRoster();
    }

    public void RemoveFromRoster(int monsterId)
    {
        roster.Remove(monsterId);

        slotsUsed = roster.Keys.Count;

        slotTracker.text = "Slots: " + slotsUsed + "/" + currentSlots;

        ShowUpdatedRoster();
    }

    // Update is called once per frame
    void Update()
    {
        if (deploying)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject.tag == "SpawnArea")
                    {
                        MonsterCard mc = roster[monsterToDeploy];

                        GameObject depMon = Instantiate(mc.monsterPrefab, new Vector3(hit.point.x, 1f, hit.point.z), Quaternion.identity);
                        Monster deployedMonster = depMon.GetComponentInChildren<Monster>();

                        deployedMonster.myId = monsterToDeploy;
                        deployedMonster.sprayRange = mc.monsterRange;
                        deployedMonster.sprayPower = mc.monsterPower;
                        deployedMonster.speed = mc.monsterSpeed;
                        deployedMonster.FilterColor = mc.color;

                        mc.isDeployed = true;
                        EventManager.TriggerEvent("DeployedMonster", new MonEvent { monsterId = monsterToDeploy });

                        deploying = false;
                    }
                }
            }
        }
    }

    public void ShowUpdatedRoster()
    {
        EventManager.TriggerEvent("DeleteCurrentCards", null);

        float cardPosX = cardStartPos;

        foreach (int monsterId in roster.Keys)
        {
            MonsterCard monster = roster[monsterId];

            GameObject monButt = Instantiate(monsterButton, canvas.transform);
            monButt.GetComponent<RectTransform>().anchoredPosition = new Vector3(cardPosX, -125, 0);
            MonsterButton monsterButt = monButt.GetComponent<MonsterButton>();

            monsterButt.monName.text = monster.monsterName;
            monsterButt.monsterId = monsterId;
            monsterButt.monSpeed.text = $"Speed: {monster.monsterSpeed}";
            monsterButt.monRange.text = $"Range: {monster.monsterRange}";
            monsterButt.monPower.text = $"Power: {monster.monsterPower}";
            monsterButt.monNotes.text = $"Notes:\n {monster.monsterNotes}";

            monsterButt.monSpeed.text = "Speed: " + monster.monsterSpeed;
            monsterButt.monRange.text = "Range: " + monster.monsterRange;
            monsterButt.monPower.text = "Power: " + monster.monsterPower;
            monsterButt.monNotes.text = "Notes:\n" + monster.monsterNotes;

            monsterButt.monPic.color = monster.color;

            monsterButt.monsterCost = monster.monsterSellPrice;
            monsterButt.monSell.GetComponentInChildren<Text>().text = "Sell: $" + monster.monsterSellPrice;

            if (monster.isDeployed)
            {
                monsterButt.ds = MonsterButton.DeployState.deployed;
                monsterButt.monDeploy.GetComponentInChildren<Text>().text = "Recall";
            }
            else
            {
                monsterButt.ds = MonsterButton.DeployState.notDeployed;
                monsterButt.monDeploy.GetComponentInChildren<Text>().text = "Deploy";
            }

            cardPosX += cardOffset;
        }

        for(int i = 0; i < currentSlots - slotsUsed; i++)
        {
            GameObject tempButt = Instantiate(emptySlotCard, canvas.transform);
            tempButt.GetComponent<RectTransform>().anchoredPosition = new Vector3(cardPosX, -125, 0);

            cardPosX += cardOffset;
        }

        if(currentSlots < maxSlots)
        {
            GameObject slotButt = Instantiate(BuyNewSlotCard, canvas.transform);
            slotButt.GetComponent<RectTransform>().anchoredPosition = new Vector3(cardPosX, -125, 0);
            BuySlotCard slotButton = slotButt.GetComponent<BuySlotCard>();

            if(currentSlots == 1)
            {
                slotButton.cost = 10;
            }
            else if(currentSlots == 2)
            {
                slotButton.cost = 15;
            }
            else if (currentSlots == 3)
            {
                slotButton.cost = 20;
            }
            else if (currentSlots == 4)
            {
                slotButton.cost = 30;
            }
            else if (currentSlots == 5)
            {
                slotButton.cost = 40;
            }
            else if (currentSlots == 6)
            {
                slotButton.cost = 50;
            }
            else
            {
                slotButton.cost = 100;
            }

            EventManager.TriggerEvent("UpdateBuyPower", new MoneyEvent { money = PlayerMoney, slots = (slotsUsed < currentSlots) });
        }
    }

    public void BuyASlot(EventBody eb)
    {
        int slotCost = ((BuyEvent)eb).monsterSellPrice;

        currentSlots += 1;
        slotsUsed = roster.Keys.Count;
        slotTracker.text = "Slots: " + slotsUsed + "/" + currentSlots;

        ShowUpdatedRoster();
        AddMoney(-slotCost);
        aSource.clip = money2;
        aSource.Play();
    }

    public void HideRoster()
    {
        EventManager.TriggerEvent("DeleteCurrentCards", null);
        StoreBox.SetActive(false);
        RosterBox.SetActive(false);
    }

    public void PrepareToDeploy(EventBody eb)
    {
        int monsterId = ((MonEvent)eb).monsterId;

        if (!roster[monsterId].isDeployed)
        {
            deploying = true;
            monsterToDeploy = monsterId;
        }
    }

    public void CancelDeploy(EventBody eb)
    {
        deploying = false;
    }

    public void RecallMonster(EventBody eb)
    {
        deploying = false;
        int monsterId = ((MonEvent)eb).monsterId;

        if (roster[monsterId].isDeployed)
        {
            roster[monsterId].isDeployed = false;
        }
    }
    public void ResetRoster(EventBody eb)
    {
        foreach (MonsterCard mc in roster.Values)
        {
            mc.isDeployed = false;
        }

        ShowUpdatedRoster();

        StoreBox.SetActive(true);
        RosterBox.SetActive(true);
        ShowStore(Random.Range(4,7));
        AddMoney(0);
    }
    
    public void ShowStore(int numOfMonsters)
    {
        float levelScaler = levelStoreScaler * GameManager.Instance.level;

        float cardPosX = cardStartPos;

        for(int i = 0;  i < numOfMonsters; i++)
        {
            MonsterCard monster = GenerateStoreCard(levelScaler);

            GameObject monButt = Instantiate(monsterStoreButton, canvas.transform);
            monButt.GetComponent<RectTransform>().anchoredPosition = new Vector3(cardPosX, 125, 0);

            MonsterButtonStore monsterButt = monButt.GetComponent<MonsterButtonStore>();

            monsterButt.monCard = monster;

            monsterButt.cost = monster.monsterBuyPrice;
            monsterButt.monName.text = monster.monsterName;
            monsterButt.monSpeed.text = "Speed: " + monster.monsterSpeed;
            monsterButt.monRange.text = "Range: " + monster.monsterRange;
            monsterButt.monPower.text = "Power: " + monster.monsterPower;
            monsterButt.monNotes.text = "Notes:\n" + monster.monsterNotes;
            monsterButt.monPic.color = monster.color;

            cardPosX += cardOffset;
        }
    }

    public MonsterCard GenerateStoreCard(float scale)
    {
        MonsterClasses mc = possibleClasses[Random.Range(0, possibleClasses.Count)];

        MonsterCard storeMonster = new MonsterCard
        {
            isDeployed = false,
            monsterName = PickAName(),
            monsterSpeed = (float)System.Math.Round(Random.Range(mc.minSpeed + (scale / 4f), mc.maxSpeed + (scale / 2f)), 1), // values were 1 and 2
            monsterRange = (float)System.Math.Round(Random.Range(mc.minRange + (scale / 8f), mc.maxRange + (scale / 8f)), 1), // values were 0.3 and 0.
            monsterPower = (float)System.Math.Round(Random.Range(mc.minPower + (scale / 2f), mc.maxPower + scale), 1), // values were 2 and 2
            monsterNotes = mc.classNotes,
            monsterBuyPrice = Random.Range(5 + GameManager.Instance.level, 10 + GameManager.Instance.level),
            monsterSellPrice = Random.Range(1 + GameManager.Instance.level, 5 + GameManager.Instance.level),
            monsterPrefab = mc.classPrefab,
            color = mc.filterColor
        };

        return storeMonster;
    }

    public void SellAMonster(EventBody eb)
    {
        BuyEvent be = (BuyEvent)eb;

        RemoveFromRoster(be.monsterId);

        AddMoney(be.monsterSellPrice);
    }

    public void BuyAMonster(EventBody eb)
    {
        BuyEvent be = (BuyEvent)eb;

        MonsterCard newMonster = new MonsterCard
        {
            isDeployed = false,
            monsterName = be.monsterName,
            monsterSpeed = be.monsterSpeed,
            monsterRange = be.monsterRange,
            monsterPower = be.monsterPower,
            monsterNotes = be.monsterNotes,
            monsterBuyPrice = be.monsterBuyPrice,
            monsterSellPrice = be.monsterSellPrice,
            monsterPrefab = be.monsterPrefab,
            color = be.color
        };

        AddToRoster(newMonster);
        aSource.clip = money1;
        aSource.Play();
        AddMoney(-be.monsterBuyPrice);
    }

    public string PickAName()
    {
        if(names.Count < 1)
        {
            GenerateNames();
        }

        string nameToPick = names[Random.Range(0, names.Count)];
        names.Remove(nameToPick);


        return nameToPick;
    }

    public void GenerateClasses()
    {
        possibleClasses = new List<MonsterClasses>();

        MonsterClasses mc1 = new MonsterClasses {maxSpeed = 2, minSpeed = 1, maxRange = 0.6f, minRange = 0.3f, minPower = 2, maxPower = 2, classNotes = "Fights fires with all their heart.", classPrefab = (GameObject)Resources.Load("DefaultMonster"), filterColor = Color.white };
        MonsterClasses mc2 = new MonsterClasses {maxSpeed = 1.5f, minSpeed = 0.7f, maxRange = 0.6f, minRange = 0.3f, minPower = 2, maxPower = 2, classNotes = "Teleports to the center of action.", classPrefab = (GameObject)Resources.Load("FarAwayMonster"), filterColor = new Color(1.0f, 1.0f, 0.59f) };
        MonsterClasses mc3 = new MonsterClasses {maxSpeed = 1.3f, minSpeed = 0.5f, maxRange = 2.8f, minRange = 1.8f, minPower = 1, maxPower = 1, classNotes = "Long-Ranged attack but doesn't hit hard.", classPrefab = (GameObject)Resources.Load("DefaultMonster"), filterColor = new Color(0.59f, 1.0f, 0.59f) };
        MonsterClasses mc4 = new MonsterClasses {maxSpeed = 0.9f, minSpeed = 0.4f, maxRange = 0.6f, minRange = 0.3f, minPower = 3, maxPower = 2.8f, classNotes = "Rescues the first people they see.", classPrefab = (GameObject)Resources.Load("Rescuer"), filterColor = new Color(0.59f, 0.59f, 0.59f) };

        possibleClasses.Add(mc1);
        possibleClasses.Add(mc2);
        possibleClasses.Add(mc3);
        possibleClasses.Add(mc4);
    }

    public void GenerateNames()
    {
        names = new List<string>();

        names.Add("Adam");
        names.Add("Chris");
        names.Add("Sam");
        names.Add("Martin");
        names.Add("Joe");
        names.Add("Alex");
        names.Add("Caitlyn");
        names.Add("Taku");
        names.Add("Nick");
        names.Add("Jack");
        names.Add("Kate");
        names.Add("Jill");
        names.Add("Claire");
        names.Add("John");
        names.Add("Oliver");
        names.Add("Charlie");
        names.Add("Emma");
        names.Add("Richard");
        names.Add("Bella");
        names.Add("Logan");
        names.Add("James");
        names.Add("Will");
        names.Add("Ava");
        names.Add("Ana");
        names.Add("Mike");
        names.Add("Ben");
        names.Add("David");
        names.Add("Lucas");
        names.Add("Monica");
        names.Add("Isaac");
        names.Add("Ryan");
        names.Add("Luke");
        names.Add("Grace");
        names.Add("Emily");
        names.Add("Chloe");
        names.Add("Lily");
        names.Add("Abby");
        names.Add("Luna");
        names.Add("Zoe");
        names.Add("Leah");
        names.Add("Taylor");
        names.Add("Violet");
        names.Add("Ash");
        names.Add("Max");
        names.Add("Molly");
        names.Add("Jade");
        names.Add("Kitty");
        names.Add("Ivy");
        names.Add("Sara");
        names.Add("Amy");
        names.Add("Al");
        names.Add("Jo");
        names.Add("Rose");
    }
}
