﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.StateManager
{
    public class SetupState : State
    {
        public SetupState()
        {
            name = "Setup";
        }
        
        private float elapsedTime;
        private float setupTime = Globals.SETUP_TIME;

        private bool playedBoop1;
        private bool playedBoop2;
        private bool playedBoop3;
        private Text text;

        public override bool checkForTransition()
        {
            return elapsedTime >= setupTime;
        }

        public override void setup()
        {
            playedBoop1 = false;
            playedBoop2 = false;
            playedBoop3 = false;
            UIManager.Instance.SetupDisplay.gameObject.SetActive(true);

            if(GameManager.Instance.level < 10)
            {
                UIManager.Instance.LevelCounter.GetComponent<Text>().text = "Deploy Phase " + GameManager.Instance.level;
            }
            else
            {
                UIManager.Instance.LevelCounter.GetComponent<Text>().text = "Deploy Phase DOOM";
            }
            
            elapsedTime = 0.0f;
            text = UIManager.Instance.CountDownText.GetComponent<Text>();
            GameManager.Instance.PrepareSimulation();
            //hide and show UI
        }

        public override void update()
        {
            elapsedTime += Time.deltaTime;
            //Set Elapsed Time UI = timeAsCountdown.ToString();
            text.text = "Deploy Ends in " + timeAsCountdown.ToString() + " Seconds...";
            if (!playedBoop1 && elapsedTime > setupTime - 2.0f)
            {
                playedBoop1 = true;
                StateManager.Instance.aSource.clip = StateManager.Instance.boop1;
                StateManager.Instance.aSource.Play();
            }
            if (!playedBoop2 && elapsedTime > setupTime - 1.0f)
            {
                playedBoop2 = true;
                StateManager.Instance.aSource.clip = StateManager.Instance.boop1;
                StateManager.Instance.aSource.Play();
            }
            if (!playedBoop3 && elapsedTime > setupTime)
            {
                playedBoop3 = true;
                StateManager.Instance.aSource.clip = StateManager.Instance.boop2;
                StateManager.Instance.aSource.Play();
            }
        }

        public override void cleanUp()
        {
            UIManager.Instance.SetupDisplay.gameObject.SetActive(false);
        }

        private int timeAsCountdown => Mathf.CeilToInt(setupTime - elapsedTime);
    }
}
