﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class PeopleIndicatorElement : MonoBehaviour
    {
        public int buildingId;

        public void Awake()
        {
            EventManager.StartListening("EndDebrief", Clear);
            EventManager.StartListening("Rescue", Rescue);
        }

        public void Clear(EventBody eb)
        {
            Destroy(gameObject);
        }

        public void Rescue(EventBody eb)
        {
            if(((RescueEvent)eb).buildingId == buildingId)
            {
                Destroy(gameObject);
            }
        }
    }
}
