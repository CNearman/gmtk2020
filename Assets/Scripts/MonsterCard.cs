﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterCard
{
    public bool isDeployed;
    public string monsterName;

    public float monsterSpeed;
    public float monsterRange;
    public float monsterPower;
    public string monsterNotes;
    public int monsterBuyPrice;
    public int monsterSellPrice;
    public GameObject monsterPrefab;
    public Color color;
}
