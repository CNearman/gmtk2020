﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject building;

    public int level;

    public int totalHospitlized;

    public int hospitalLimit;

    public int sizeX;
    public int sizeZ;
    public float offsetX;
    public float offsetZ;
    public float levelOffsetX;
    public float LevelOffsetZ;

    public int fireCount;

    public float fireHealthMin;
    public float fireHealthMax;
    public float fireHealthLevelScaler;

    public float fireStrength;

    public float buildingHealthMin;
    public float buildingHealthMax;
    public int buildingPopMin;
    public int buildingPopMax;

    public IList<Building> Buildings;
    public IList<Building> ActiveBuildings;
    public IList<int> BuildingsOnFire;

    public int roundStartingPopulation;

    public float RoundTimer;
    private bool shouldCountTime;

    public List<AudioClip> BurndownClips;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }

        level = 1;
        totalHospitlized = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        Buildings = new List<Building>();
        ActiveBuildings = new List<Building>();
        BuildingsOnFire = new List<int>();
        CreateBuildings(fireCount);
    }

    public void PrepareSimulation()
    {
        Reset();
        //BuildingsOnFire = ChooseOnFireBuildings(fireCount, sizeX * sizeZ);

        int numOfFire;
        if(level > 9)
        {
            numOfFire = 12;
        }
        else if(level > 7)
        {
            numOfFire = 7;
        }
        else if(level > 4)
        {
            numOfFire = 6;
        }
        else if(level > 3)
        {
            numOfFire = 4;
        }
        else if (level > 2)
        {
            numOfFire = 3;
        }
        else if(level > 1)
        {
            numOfFire = 2;
        }
        else
        {
            numOfFire = 1;
        }

        fireCount = numOfFire;

        BuildingsOnFire = ChooseOnFireBuildings(numOfFire, sizeX * sizeZ);
        SetOnFireBuildings();
    }

    public void CreateBuildings(int numberOfFires)
    {
        int totalBuildings = sizeX * sizeZ;

        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                int choice = i + (j * sizeX);

                CreateBuilding(i * offsetX + levelOffsetX, j * offsetZ + LevelOffsetZ, choice);
            }
        }
    }

    private void CreateBuilding(float posX, float posZ, int id)
    {
        GameObject currentBuilding = Instantiate(building, new Vector3(posX, 1, posZ), Quaternion.Euler(90,0,0));
        Building build = currentBuilding.GetComponent<Building>();
        build.SetId(id);
        Buildings.Add(build);
    }

    private IList<int> ChooseOnFireBuildings(int numberOfFires, int totalBuildings)
    {
        List<int> buildingsOnFire = new List<int>();
        for (int i = 0; i < numberOfFires; i++)
        {
            int choice = Random.Range(0, totalBuildings);

            while (buildingsOnFire.Contains(choice))
            {
                choice += 1;
                if (choice >= totalBuildings)
                {
                    choice = 0;
                }
            }

            buildingsOnFire.Add(choice);

        }

        return buildingsOnFire;
    }

    private void SetOnFireBuildings()
    {
        float levelScaler = fireHealthLevelScaler * level;

        foreach (Building build in Buildings)
        {
            if (BuildingsOnFire.Contains(build.id))
            {
                build.SetStats(Random.Range(fireHealthMin + levelScaler, fireHealthMax + levelScaler), fireStrength, Random.Range(buildingHealthMin, buildingHealthMax), Random.Range(buildingPopMin, buildingPopMax));
                roundStartingPopulation += build.people;
                build.StartFire();
                ActiveBuildings.Add(build);
            }
        }
    }

    private void Reset()
    {
        foreach (Building build in Buildings)
        {
            build.Reset();
        }
        RoundTimer = 0.0f;
        BuildingsOnFire.Clear();
        ActiveBuildings.Clear();
        roundStartingPopulation = 0;
    }

    public void BeginTimer()
    {
        shouldCountTime = true;
    }

    public void StopTimer()
    {
        shouldCountTime = false;
    }

    public bool ShouldSimulationEnd()
    {
        bool result = true;
        foreach (Building build in ActiveBuildings)
        {
            if(!build.isBurnedDown && !build.isPutOut)
            {
                result = false;
            }
        }

        return result;
    }


    public RoundSummary GetRoundSummary()
    {
        totalHospitlized += ActiveBuildings.Where(x => x.isBurnedDown).Sum(x => x.people);

        level++;
        RoundSummary rs = new RoundSummary();
        rs.BuildingsLost = ActiveBuildings.Where(x => x.isBurnedDown).Count();
        rs.PeopleHospitalized = ActiveBuildings.Where(x => x.isBurnedDown).Sum(x => x.people);
        rs.TimeElapsed = Mathf.Floor(RoundTimer);
        rs.MoneyEarned = (fireCount - rs.BuildingsLost) * Globals.MONEY_FOR_BUILDING_SAVED + (roundStartingPopulation - rs.PeopleHospitalized) * Globals.MONEY_FOR_PERSON_SAVED;
        return rs;
    }


    // Update is called once per frame
    public void runSimulation()
    {
        if (shouldCountTime)
        {
            RoundTimer += Time.deltaTime;
        }
        foreach(Building build in ActiveBuildings)
        {
            build.Run();
        }
    }
}
