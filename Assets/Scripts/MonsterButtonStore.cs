﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterButtonStore : MonoBehaviour
{
    public Image monPic;
    public Text monName;
    public Text monSpeed;
    public Text monRange;
    public Text monPower;
    public Text monNotes;

    public int cost;

    public Button monBuy;

    public MonsterCard monCard;

    bool purchased;

    void Awake()
    {
        EventManager.StartListening("UpdateBuyPower", UpdateBuyPower);
        EventManager.StartListening("StartSimulation", CloseStore);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateBuyPower(EventBody eb)
    {
        if(!purchased)
        {
            int money = ((MoneyEvent)eb).money;
            bool slots = ((MoneyEvent)eb).slots;

            if(slots)
            {
                if (money >= cost)
                {
                    monBuy.interactable = true;
                    monBuy.GetComponentInChildren<Text>().text = "Recruit: $" + cost;
                }
                else
                {
                    monBuy.interactable = false;
                    monBuy.GetComponentInChildren<Text>().text = "Cost: $" + cost;
                }
            }
            else
            {
                monBuy.interactable = false;
                monBuy.GetComponentInChildren<Text>().text = "No Slots: $" + cost;
            }
            
        }
    }

    public void BuyMonster()
    {
        purchased = true;
        monBuy.interactable = false;
        monBuy.GetComponentInChildren<Text>().text = "Purchased!";
        EventManager.TriggerEvent("BuyAMonster", new BuyEvent {
            monsterName = monCard.monsterName,
            monsterSpeed = monCard.monsterSpeed,
            monsterRange = monCard.monsterRange,
            monsterPower = monCard.monsterPower,
            monsterNotes = monCard.monsterNotes,
            monsterBuyPrice = monCard.monsterBuyPrice,
            monsterSellPrice = monCard.monsterSellPrice,
            monsterPrefab = monCard.monsterPrefab,
            color = monCard.color
        });
    }

    public void CloseStore(EventBody eb)
    {
        Destroy(gameObject);
    }
}