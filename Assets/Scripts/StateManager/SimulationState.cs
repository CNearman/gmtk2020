﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.StateManager
{
    public class SimulationState : State
    {
        public SimulationState()
        {
            name = "Simulation";
        }

        public override bool checkForTransition()
        {
            return GameManager.Instance.ShouldSimulationEnd();
        }

        public override void setup()
        {
            //hide and show UI
            GameManager.Instance.BeginTimer();
            EventManager.TriggerEvent("StartSimulation", null);
            PlayerManager.Instance.HideRoster();
        }

        public override void update()
        {
            //Run simulation
            GameManager.Instance.runSimulation();
        }

        public override void cleanUp()
        {
            GameManager.Instance.StopTimer();
            EventManager.TriggerEvent("EndSimulation", null);
        }
    }
}
