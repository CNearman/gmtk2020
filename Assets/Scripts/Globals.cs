﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public static class Globals
    {
        public static float SETUP_TIME = 20.0f;
        public static int MONEY_FOR_BUILDING_SAVED = 3;
        public static int MONEY_FOR_PERSON_SAVED = 1;
    }
}
