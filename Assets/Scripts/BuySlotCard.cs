﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuySlotCard : MonoBehaviour
{
    public Button buyButton;
    public int cost;

    private void Awake()
    {
        EventManager.StartListening("DeleteCurrentCards", DeleteCard);
        EventManager.StartListening("UpdateBuyPower", UpdateBuyPower);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DeleteCard(EventBody eb)
    {
        Destroy(gameObject);
    }

    void UpdateBuyPower(EventBody eb)
    {
        int money = ((MoneyEvent)eb).money;
        bool slots = ((MoneyEvent)eb).slots;

        if (money >= cost)
        {
            buyButton.interactable = true;
            buyButton.GetComponentInChildren<Text>().text = "Buy\n$" + cost;
        }
        else
        {
            buyButton.interactable = false;
            buyButton.GetComponentInChildren<Text>().text = "Cost\n$" + cost;
        }
    }

    public void BuyASlot()
    {
        EventManager.TriggerEvent("BuyASlot", new BuyEvent { monsterId = -1, monsterSellPrice = cost });
    }
}
