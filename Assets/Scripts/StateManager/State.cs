﻿using Assets.Scripts.StateManager;

public abstract class State
{
    public static State SETUP = new SetupState();
    public static State SIMULATION = new SimulationState();
    public static State DEBRIEF = new DebriefState();

    public State targetState;
    public string name;

    public abstract bool checkForTransition();
    public abstract void update();
    public abstract void setup();
    public abstract void cleanUp();
}