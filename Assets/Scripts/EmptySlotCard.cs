﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptySlotCard : MonoBehaviour
{
    private void Awake()
    {
        EventManager.StartListening("DeleteCurrentCards", DeleteCard);
    }

    void DeleteCard(EventBody eb)
    {
        Destroy(gameObject);
    }
}
