﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public bool isOnFire;
    public bool isPutOut;
    public bool isBurnedDown;
    public float fireStrength;
    public float fireStartingHealth;
    public float fireHealth;
    public float buildingHealth;
    public int people;

    public int id;

    public List<BuildingChild> childPoints;
    public List<AudioClip> BurndownClips;

    public int SpriteIndex;
    public List<Sprite> BuildingSprites;
    public List<Sprite> BuildingSavedSprites;
    public List<Sprite> FireLevel1Sprites;
    public List<Sprite> FireLevel2Sprites;
    public List<Sprite> FireLevel3Sprites;
    public List<Sprite> FireLevel4Sprites;
    public Sprite BurnedDownSprite;
    public GameObject PeoplePrefab;
    public List<GameObject> People;

    private AudioSource source;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        isPutOut = false;
        isBurnedDown = false;
        source = gameObject.GetComponent<AudioSource>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        SpriteIndex = Random.Range(0, BuildingSprites.Count);
        spriteRenderer.sprite = BuildingSprites[SpriteIndex];
        childPoints = new List<BuildingChild>();
        childPoints.AddRange(gameObject.transform.GetComponentsInChildren<BuildingChild>());
        foreach (BuildingChild cPoint in childPoints)
        {
            cPoint.SetFireDeactive();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public void Run()
    {
        // If ya on fire, ya burn
        if(isOnFire)
        {
            UpdateFireHealthImage();
            if (fireHealth <= 0)
            {
                PutOut();
            }
            else
            {
                if (buildingHealth <= 0f)
                {
                    BurnDown();
                }
                else
                {
                    buildingHealth -= fireStrength * Time.deltaTime;
                }
            }
        }
    }

    private void UpdateFireHealthImage()
    {
        if(fireHealth < 10)
        {
            spriteRenderer.sprite = FireLevel1Sprites[SpriteIndex];
        }
        else if(fireHealth < 15)
        {
            spriteRenderer.sprite = FireLevel2Sprites[SpriteIndex];
        }
        else if (fireHealth < 2)
        {
            spriteRenderer.sprite = FireLevel3Sprites[SpriteIndex];
        }
        else
        {
            spriteRenderer.sprite = FireLevel4Sprites[SpriteIndex];
        }
    }

    public void SetId(int idP)
    {
        id = idP;
    }

    public void SetStats(float fireHealthP, float fireStrengthP, float buildingHealthP, int peopleP)
    {
        fireStrength = fireStrengthP;
        fireHealth = fireHealthP;
        fireStartingHealth = fireHealth;
        buildingHealth = buildingHealthP;
        people = peopleP;
    }

    public void StartFire()
    {
        isOnFire = true;

        //spriteRenderer.color = Color.red;
        UpdateFireHealthImage();
        foreach (BuildingChild cPoint in childPoints)
        {
            cPoint.SetFireActive();
        }
        for(int i = 0; i < people; i++)
        {
            float offset = i + 1;
            /*if(people % 2 == 0) //even
            {
                offset += -0.5f;
            }*/
            GameObject people = Instantiate(PeoplePrefab, transform.position + new Vector3(0.5f  + (offset * 0.2f) , 1, -1), Quaternion.Euler(90, 0, 0), transform);
            people.GetComponent<PeopleIndicatorElement>().buildingId = id;
        }
    }

    public void GetSprayed(float power)
    {
        fireHealth -= power;
    }

    public void BurnDown()
    {
        isOnFire = false;
        isBurnedDown = true;
        foreach (BuildingChild cPoint in childPoints)
        {
            cPoint.SetFireDeactive();
        }
        spriteRenderer.sprite = BurnedDownSprite;
        int clipIndex = Random.Range(0, BurndownClips.Count);
        Debug.Log("Playing Burndown Clip " + clipIndex);
        source.clip = BurndownClips[clipIndex];
        source.Play();
    }

    public void PutOut()
    {
        isOnFire = false;
        isPutOut = true;
        foreach (BuildingChild cPoint in childPoints)
        {
            cPoint.SetFireDeactive();
        }

        spriteRenderer.sprite = BuildingSavedSprites[SpriteIndex];
    }

    public void Reset()
    {
        isOnFire = false;
        isPutOut = false;
        isBurnedDown = false;
        foreach (BuildingChild cPoint in childPoints)
        {
            cPoint.SetFireDeactive();
        }
        spriteRenderer.color = Color.white;
        spriteRenderer.sprite = BuildingSprites[SpriteIndex];
    }

    public void RescuePeople()
    {
        people = 0;
        EventManager.TriggerEvent("Rescue", new RescueEvent() { buildingId = id });
    }
}
