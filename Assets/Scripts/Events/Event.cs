﻿// These represent the various event information. Event body is the base class, then depending on what information needs to be passed in the event, a different event body is used. Example ones are below.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBody
{

}

public class MonEvent : EventBody
{
    public int monsterId;
}

public class MoneyEvent : EventBody
{
    public int money;
    public bool slots;
}

public class BuyEvent : EventBody
{
    public string monsterName;
    public float monsterSpeed;
    public float monsterRange;
    public float monsterPower;
    public string monsterNotes;
    public int monsterBuyPrice;
    public int monsterSellPrice;
    public int monsterId;
    public GameObject monsterPrefab;
    public Color color;
}

public class RescueEvent : EventBody
{
    public int buildingId;
}