﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterButton : MonoBehaviour
{
    public Image monPic;
    public Text monName;
    public Text monSpeed;
    public Text monRange;
    public Text monPower;
    public Text monNotes;

    public Button monDeploy;
    public Button monSell;

    public int monsterId;
    public int monsterCost;
        
    public enum DeployState {
        notDeployed,
        deploying,
        deployed
    }

    public DeployState ds;

    void Awake()
    {
        EventManager.StartListening("DeleteCurrentCards", DeleteCard);
        EventManager.StartListening("DeployedMonster", DeployedMonster);

        EventManager.StartListening("PrepareToDeploy", CancelDeploy);
        EventManager.StartListening("RecallMonster", CancelDeploy);
    }

    void DeleteCard(EventBody eb)
    {
        Destroy(gameObject);
    }

    void DeployedMonster(EventBody eb)
    {
        if(monsterId == ((MonEvent)eb).monsterId)
        {
            ds = DeployState.deployed;
            monDeploy.GetComponentInChildren<Text>().text = "Recall";
        }
    }

    void CancelDeploy(EventBody eb)
    {
        if (monsterId != ((MonEvent)eb).monsterId && ds == DeployState.deploying)
        {
            ds = DeployState.notDeployed;
            monDeploy.GetComponentInChildren<Text>().text = "Deploy";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeployButton()
    {

        if (ds == DeployState.notDeployed)
        {
            EventManager.TriggerEvent("PrepareToDeploy", new MonEvent { monsterId = monsterId });
            ds = DeployState.deploying;
            monDeploy.GetComponentInChildren<Text>().text = "Cancel";
        }
        else if(ds == DeployState.deploying)
        {
            EventManager.TriggerEvent("CancelDeploy", new MonEvent { monsterId = monsterId });
            ds = DeployState.notDeployed;
            monDeploy.GetComponentInChildren<Text>().text = "Deploy";
        }
        else if(ds == DeployState.deployed)
        {
            EventManager.TriggerEvent("RecallMonster", new MonEvent { monsterId = monsterId });
            ds = DeployState.notDeployed;
            monDeploy.GetComponentInChildren<Text>().text = "Deploy";
        }
    }

    public void SellButton()
    {
        EventManager.TriggerEvent("RecallMonster", new MonEvent { monsterId = monsterId });
        EventManager.TriggerEvent("SellAMonster", new BuyEvent { monsterId = monsterId, monsterSellPrice = monsterCost });
    }
}
