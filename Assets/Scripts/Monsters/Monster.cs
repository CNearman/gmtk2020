﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : MonoBehaviour
{
    public int myId;

    public float speed;
    public float sprayRange;
    public float sprayPower;
    public GameObject ParticleSystem;

    protected bool hasTarget;
    protected GameObject targetFire;
    protected SpriteRenderer spriteRenderer;
    protected ParticleSystem ps;

    public Color FilterColor;
    public bool colorSet = false;

    protected NavMeshAgent navAgent;
    protected bool shouldGo;

    public GameObject parentGO;

    protected Animator anim;
    public List<AudioClip> SprayClips;
    public AudioSource source;

    void Awake()
    {
        EventManager.StartListening("RecallMonster", RecallMonster);
        EventManager.StartListening("StartSimulation", Start);
        EventManager.StartListening("StartDebrief", End);
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        navAgent = gameObject.GetComponentInParent<NavMeshAgent>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        ps = ParticleSystem.GetComponent<ParticleSystem>();
        ps.Stop();
        source = gameObject.GetComponent<AudioSource>();
        int clipIndex = UnityEngine.Random.Range(0, SprayClips.Count);
        source.clip = SprayClips[clipIndex];
        navAgent.speed = speed;
        navAgent.enabled = false;
        shouldGo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!colorSet)
        {
            colorSet = true;
            spriteRenderer.color = FilterColor;
        }
        if (shouldGo)
        {
            PerformDuties();
        }
    }   

    public virtual void PerformDuties()
    {
        Vector3 monsterPosition = transform.position;
        float distanceToTarget = Mathf.Infinity;

        // Get closest fire
        GameObject[] fires = GameObject.FindGameObjectsWithTag("Fire"); // probably dont want to do this every frame. should probably also be static class and not done on every object

        if (fires.Length > 0)
        {
            hasTarget = true;

            foreach (GameObject fire in fires)
            {
                Vector3 difference = fire.transform.position - monsterPosition;
                float distance = difference.magnitude;
                if (distance < distanceToTarget)
                {
                    targetFire = fire;
                    distanceToTarget = distance;
                }
            }
        }
        else
        {
            hasTarget = false;
        }

        anim.SetBool("WalkDown", false);
        anim.SetBool("WalkRight", false);
        anim.SetBool("WalkLeft", false);
        anim.SetBool("Idle", false);
        anim.SetBool("Spray", false);

        if (hasTarget)
        {
            // If fire is in spray range, spray
            if (distanceToTarget < sprayRange)
            {
                navAgent.isStopped = true;
                targetFire.GetComponent<BuildingChild>().GetSprayed(sprayPower * Time.deltaTime);
                anim.SetBool("Spray", true);
                var displacement = targetFire.GetComponent<BuildingChild>().daddy.transform.position - monsterPosition;
                if (!source.isPlaying)
                {
                    source.Play();
                }
                if (!ps.isPlaying)
                {
                    ps.Play();
                }
                if (displacement.x < 0)
                {
                    spriteRenderer.flipX = true;
                    if(displacement.y < 0) // 
                    {
                        ParticleSystem.transform.rotation = Quaternion.Euler(0, -135 , 0);
                    }
                    else
                    {
                        ParticleSystem.transform.rotation = Quaternion.Euler(0, -45, 0);
                    }
                }
                else
                {
                    spriteRenderer.flipX = false;
                    if (displacement.y > 0) // 
                    {
                        ParticleSystem.transform.rotation = Quaternion.Euler(0, 135, 0);
                    }
                    else
                    {
                        ParticleSystem.transform.rotation = Quaternion.Euler(0, 45, 0);
                    }
                }
            }
            else // Else, head toward fire
            {
                if (source.isPlaying)
                {
                    source.Stop();
                }
                navAgent.isStopped = false;
                navAgent.SetDestination(targetFire.transform.position);

                if (ps.isPlaying)
                {
                    ps.Stop();
                }

                anim.SetBool("WalkDown", false);
                anim.SetBool("WalkRight", false);
                anim.SetBool("WalkLeft", false);
                anim.SetBool("Idle", false);
                var x = navAgent.velocity.x;
                var z = navAgent.velocity.z;
                if(Math.Abs(z) > Math.Abs(x))
                {
                    if(z < 0) // going down
                    {
                        anim.SetBool("WalkDown", true);
                    }
                    else
                    {
                        if(x > 0 )
                        {
                            anim.SetBool("WalkRight", true);
                        }
                        else
                        {
                            anim.SetBool("WalkLeft", true);
                        }
                    }
                }
                else if(x != 0)
                {
                    if (x > 0)
                    {
                        anim.SetBool("WalkRight", true);
                    }
                    else
                    {
                        anim.SetBool("WalkLeft", true);
                    }
                }
                else
                {
                    anim.SetBool("Idle", true);
                }
            }
        }
        else
        {
            navAgent.isStopped = true;
        }
    }

    void Start(EventBody eb)
    {
        navAgent.enabled = true;
        shouldGo = true;
    }

    void End(EventBody eb)
    {
        EventManager.StopListening("RecallMonster", RecallMonster);
        EventManager.StopListening("SimulationStart", Start);
        EventManager.StopListening("SimulationEnd", End);
        source.Stop();
        ps.Stop();
        Destroy(parentGO);
    }


    void RecallMonster(EventBody eb)
    {
        int monsterId = ((MonEvent)eb).monsterId;

        
        if (myId == monsterId)
        {
            EventManager.StopListening("RecallMonster", RecallMonster);
            EventManager.StopListening("SimulationStart", Start);
            EventManager.StopListening("SimulationEnd", End);
            Destroy(parentGO);
        }
    }
}
