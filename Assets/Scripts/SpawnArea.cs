﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea : MonoBehaviour
{
    void Awake()
    {
        EventManager.StartListening("PrepareToDeploy", PrepareToDeploy);
        EventManager.StartListening("CancelDeploy", CancelDeploy);
        EventManager.StartListening("RecallMonster", CancelDeploy);
        EventManager.StartListening("DeployedMonster", CancelDeploy);
        EventManager.StartListening("StartSimulation", CancelDeploy);
    }

    void PrepareToDeploy(EventBody eb)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    void CancelDeploy(EventBody eb)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
}
