﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FarAwayMonster : Monster
{
    private bool hasJumped = false;

    public override void PerformDuties()
    {
        if (!hasJumped)
        {
            navAgent.enabled = false;
            hasJumped = true;
            parentGO.transform.SetPositionAndRotation(new Vector3(0, parentGO.transform.position.y, parentGO.transform.position.z), parentGO.transform.rotation);
            navAgent.enabled = true;
        }
        
        base.PerformDuties();
    }
}
