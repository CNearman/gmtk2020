﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateBuildings : MonoBehaviour
{
    public GameObject building;

    public int sizeX;
    public int sizeZ;
    public float offsetX;
    public float offsetZ;
    public float levelOffsetX;
    public float LevelOffsetZ;

    public int fireCount;

    public float fireHealthMin;
    public float fireHealthMax;
    public float fireStrength;

    public float buildingHealthMin;
    public float buildingHealthMax;
    public int buildingPopMin;
    public int buildingPopMax;

    public IList<Building> Buildings;
    public IList<int> BuildingsOnFire;

    // Start is called before the first frame update
    void Start()
    {
        Buildings = new List<Building>();
        BuildingsOnFire = ChooseOnFireBuildings(fireCount, sizeX * sizeZ);
        CreateBuildings(fireCount);
        SetOnFireBuildings();
    }

    public void CreateBuildings(int numberOfFires)
    {
        int totalBuildings = sizeX * sizeZ;

        for(int i = 0; i < sizeX; i++)
        {
            for(int j = 0; j < sizeZ; j++)
            {
                int choice = i + (j * sizeX);

                CreateBuilding(i * offsetX + levelOffsetX, j * offsetZ + LevelOffsetZ, choice);
            }
        }
    }

    private void CreateBuilding(float posX, float posZ, int id)
    {
        GameObject currentBuilding = Instantiate(building, new Vector3(posX, 1, posZ), Quaternion.identity);
        Building build = currentBuilding.GetComponent<Building>();
        build.SetId(id);
        Buildings.Add(build);
    }

    private IList<int> ChooseOnFireBuildings(int numberOfFires, int totalBuildings)
    {
        List<int> buildingsOnFire = new List<int>();
        for (int i = 0; i < numberOfFires; i++)
        {
            int choice = Random.Range(0, totalBuildings);

            while (buildingsOnFire.Contains(choice))
            {
                choice += 1;
                if (choice >= totalBuildings)
                {
                    choice = 0;
                }
            }

            buildingsOnFire.Add(choice);

        }

        return buildingsOnFire;
    }

    private void SetOnFireBuildings()
    {
        foreach(Building build in Buildings)
        {
            if (BuildingsOnFire.Contains(build.id))
            {
                build.SetStats(Random.Range(fireHealthMin, fireHealthMax), fireStrength, Random.Range(buildingHealthMin, buildingHealthMax), Random.Range(buildingPopMin, buildingPopMax));
                build.StartFire(); 
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
